#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from scipy.fftpack import fft, ifft

data=np.loadtxt('../postProcessing/forceCoeffs_object/0/coefficient.dat', skiprows=9)


xl=len(data[:,0])
x=np.linspace(data[0,0],xl-1,xl)
ylim = np.ceil(max(data[1:,1]))

print(f'ymax: {ylim} size: {xl}')

title = 'C3: Drag Coe.'
offset = 2

plt.subplot(211)
plt.plot(data[offset:,0],data[offset:,1])
plt.grid()
plt.xlabel('Time', fontsize=18)
plt.ylabel('cd', fontsize=18)
plt.ylim([0, ylim])
plt.title(title, fontsize=18)


plt.subplot(212)
plt.plot(x[offset:],data[offset:,1])
plt.grid()
plt.xlabel('Iteration', fontsize=18)
plt.ylabel('cd', fontsize=18)
plt.ylim([0, ylim])


plt.tight_layout() 

plt.savefig('cd.png') 

plt.show() 
