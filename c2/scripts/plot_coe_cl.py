#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from scipy.fftpack import fft, ifft


data=np.loadtxt('../postProcessing/forceCoeffs_object/0/coefficient_0.dat', skiprows=9)


xl=len(data[:,0])
x=np.linspace(data[0,0],xl-1,xl)



ylim = np.ceil(max(data[1:,3]))

print(f'ymax: {ylim} size: {xl}')

#plot(data[:,0],data[:,3],'o')
#plt.plot(data[:,0],data[:,3])


plt.subplot(211)
plt.plot(data[:,0],data[:,3])
plt.grid()
plt.xlabel('Time', fontsize=18)
plt.ylabel('cl', fontsize=18)
#plt.ylim([0, 2])
#plt.title('Lift coe.', fontsize=18)


plt.subplot(212)
plt.plot(x,data[:,3])
plt.grid()
plt.xlabel('Iteration', fontsize=18)
plt.ylabel('cl', fontsize=18)
# #plt.ylim([0, 2])
# #plt.title('Lift coe.', fontsize=18)


plt.tight_layout() 



plt.savefig('cl.png') 

plt.show() 
